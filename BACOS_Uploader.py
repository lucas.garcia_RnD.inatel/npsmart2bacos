'''DB_Converter v1

Utilitario utilizado para conectar ao NPSmart e coletar as tabelas selecionada para importar ao BD BACOS mariaDb

1 - conectar no SPES e rodar o NPSMART_COLLECT (play na linha 15).
2 - após finalizado conectar nas duas openconnect e rodar o BACOS_UPLOADER (play na linha 9).

'''
import os
import platform
from Connections import conecta_mariadb
import pandas as pd
from datetime import datetime

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #definindo diretorios
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "media"))
    csv_path = os.path.abspath(os.path.join(directory_path, 'csv'))

    # conectando ao NPsmart
    all_tables = os.listdir(csv_path)
    
    # criando cursor do bd
    con_bacos = conecta_mariadb()
    cursor = con_bacos.cursor()

    #table dict
    table_dict = {'enodebcell_lte':'huawei_configuration',
                  'enodebfunction_sran':'huawei_configuration',
                  'cellop_sran':'huawei_configuration',
                  'cnoperatorta_sran':'huawei_configuration',
                  'ucell_rnc_wcdma':'huawei_configuration',
                  'gcell_gsm':'huawei_configuration',
                  'twampresponder_lte':'huawei_configuration',
                  'twampresponder_sran':'huawei_configuration',
                  'enodeb_lte':'huawei_configuration',
                  'ne_lte':'huawei_configuration',
                  's1_lte':'huawei_configuration',
                  's1_sran':'huawei_configuration',
                  'epgroup_lte':'huawei_configuration',
                  'epgroup_sran':'huawei_configuration',
                  'enodebsctphost_lte':'huawei_configuration',
                  'sctphost_sran':'huawei_configuration',
                  'enodebuserplanehost_lte':'huawei_configuration',
                  'userplanehost_sran':'huawei_configuration',
                  'sctplnk_rnc_wcdma':'huawei_configuration',
                  'sctplnk_nodeb_wcdma':'huawei_configuration',
                  'vw_reassembly_daily_lte':'npm'}

    print(f'>>>>>> START DATABASE UPLOAD <<<<<<<')
    start = datetime.now()
    for current_table in all_tables:

        #definindo parametros
        table = current_table.replace('.csv', '')
        schema = table_dict[table]
        current_mo_dir = f'{csv_path}\\{current_table}'
        df_csv = pd.read_csv(current_mo_dir, dtype = 'str', low_memory=False)
        list_colums = list(df_csv.columns)
        now = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        csv_done_file = os.path.abspath(os.path.join(directory_path, 'done', f'{table}_{now}.csv'))
        #salvando csv novo sem espaços em branco
        df_csv.fillna(0, inplace = True)
        df_csv.to_csv(current_mo_dir, index = None, header=True)

        #criando tabela se o comando for novo
        sql_table_creation = f'CREATE TABLE IF NOT EXISTS {schema}.{table} ('
        for column in list_colums:
            if not column == 'DATE':
                column = column.replace('.','')
                column = column.replace('/','_')
                column = column.replace('-','_')
                sql_insert_col = f"{column.replace(' ','_')} varchar(100) DEFAULT '-', "
                sql_table_creation = sql_table_creation + sql_insert_col
            else:
                column = column.replace('.','')
                column = column.replace('-','_')
                sql_insert_col = f"{column.replace(' ','_')} date DEFAULT NULL, "
                sql_table_creation = sql_table_creation + sql_insert_col

        sql_table_creation = sql_table_creation[:-2] + ');'
        cursor.execute(sql_table_creation)
        con_bacos.commit()

        #limpando tabela (DANGER ------ UTILIZAR SÓ SE FOR REALMENTE NECESSÁRIO)
        # table_clean = f'TRUNCATE {schema}.{table};'
        # cursor.execute(table_clean)
        # con_bacos.commit()

        #subindo CSV na tabela
        csv_final_path = os.path.join(csv_path, current_table)
        upload_csv_path = csv_final_path.replace('\\', '/')
        cur_platform = platform.system()
        print(f'Loading {table}...')
        if cur_platform.upper() == 'WINDOWS':
            sql = f'LOAD DATA LOCAL INFILE "{upload_csv_path}" REPLACE INTO TABLE {schema}.{table}\n' \
                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                  f'LINES TERMINATED BY \'\r\n\' ignore 1 lines;'
        else:
            sql = f'LOAD DATA LOCAL INFILE "{upload_csv_path}" REPLACE INTO TABLE {schema}.{table}\n' \
                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                  f'LINES TERMINATED BY \'\n\' ignore 1 lines;'
        cursor.execute(sql)
        con_bacos.commit()

        print(f'Done: {table}')
        os.rename(csv_final_path, csv_done_file)

    con_bacos.close()
    final = datetime.now()
    delta = final - start
    delta = str(delta).split('.')[0]
    print(f'Uploaded in {delta}.')
    print(f'>>>>>> FINISH DATABASE UPLOAD <<<<<<<')